import React from "react";
import logo from '../images/logo_pokeapi.png';
import { NavLink } from 'react-router-dom';
import './layout.scss';

const navs = [
  {
    path: '/', name: 'List'
  },
  {
    path: '/my-pokemon', name: 'My Pokemon'
  }
]

function Nav() {
  return (
    <nav className="navbar">
      <img src={logo} alt=""/>
      <ul className="nav-wrapper">
        {
          navs.map((item, idx) => (
            <li key={idx}>
              <NavLink exact to={item.path} activeClassName="activeTab">{item.name}</NavLink>
            </li>
          ))
        }
      </ul>
    </nav>
  );
}

export default Nav;