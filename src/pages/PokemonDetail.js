import React, { useState, useEffect } from 'react';
import Layout from '../layout/Layout';
import styled from '@emotion/styled';
import { getDetailPokemon } from '../utils/globalApi';

const Button = styled.button`
  padding: 12px;
  background-color: #b1d578;
  border-radius: 10px;
  border: none;
  color: #fff;
  font-weight: bold;
  width: 80%;
  &:hover {
    background-color: #9bcc50;
    cursor: pointer;
  }`;

function PokemonDetail () {
  const [detailPokemon, setDetailPokemon] = useState({});

  useEffect(() => {
    fetchDetailPokemon();
  }, []);

  const fetchDetailPokemon = () => {
    const path = window.location.search;
    const pokemonName = path.split('?')[1];
    getDetailPokemon(pokemonName).then(response => {
      const detailPokemon = response;
      setDetailPokemon(detailPokemon);
    })
  };

  const addPokemon = () => {
    const arr = JSON.parse(localStorage.getItem('my_pokemon')) || [];
    arr.push(detailPokemon);
    localStorage.setItem('my_pokemon', JSON.stringify(arr));
  }

  return (
		<Layout>
      <div className="content-container">
        <h2>{detailPokemon.name}</h2>
        <div className="pokemon-detail-wrapper">
          <div className="picture-pokemon">
            <img src={detailPokemon?.sprites?.other['official-artwork'].front_default} alt="" />
          </div>
          <Button onClick={() => addPokemon()}>ADD +</Button>
          <div className="type-pokemon">
            <h4>Type</h4>
            <ul>
              {detailPokemon.types?.map((item, idx) => (
                <li key={idx}>{item.type.name}</li>
              ))}
            </ul>
          </div>
          <div className="moves-pokemon">
            <h4>Moves</h4>
            <table className="table-container">
              <thead>
                <tr>
                  <th>Move</th>
                </tr>
              </thead>
              <tbody>
                {detailPokemon.moves?.map((item, idx) => (
                  <tr key={idx}>
                    <td>{idx + 1}. {item.move.name.replace('-',' ')}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </Layout>
	);
}

export default PokemonDetail;