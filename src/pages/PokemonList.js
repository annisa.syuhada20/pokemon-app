import React, { useState, useEffect  } from 'react';
import { getListPokemon } from '../utils/globalApi';
import Layout from '../layout/Layout';
import './global.scss';
import { Link } from 'react-router-dom';

function PokemonList () {
  const [listPokemon, setListPokemon] = useState([]);

  useEffect(() => {
    fetchListPokemon();
  }, []);

  const fetchListPokemon = () => {
    getListPokemon().then(response => {
      const listPokemon = response.results;
      setListPokemon(listPokemon);
    })
  };

  return (
    <Layout>
      <h3>Pokemon List</h3>
      <div className="pokemon-wrapper">
        {listPokemon.map((item, idx) => (
          <div className="pokemon-item" key={idx}>
            <Link to={{ pathname: '/pokemon-detail', search: item.name }} className="pokemon">
              <div className="title">{item.name}</div>
              <div className="owned">Owned : 0</div>
            </Link>
          </div>
        ))}
      </div>
    </Layout>
  );

}

export default PokemonList;
