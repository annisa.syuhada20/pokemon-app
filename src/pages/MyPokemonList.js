import React, { useState } from 'react';
import Layout from '../layout/Layout';
import styled from '@emotion/styled';

const Button = styled.button`
  float: right;
  width: 2.5rem;
  margin: 2px;
  padding: 12px;
  background-color: transparent;
  border-radius: 50%;
  border: none;
  &:hover {
    background-color: #AFAFAF;
    cursor: pointer;
  }`;

function MyPokemonList () {

  const [myPokemonList, setPokemonList] = useState(() => {
    const saved = localStorage.getItem('my_pokemon');
    const myPokemon = JSON.parse(saved);
    return myPokemon || [];
  });

  const removePokemon = (name) => {
    const arr = JSON.parse(localStorage.getItem('my_pokemon')) || [];
    const removePokemon = arr.filter((pokemon) => pokemon.name !== name);
    localStorage.setItem('my_pokemon', JSON.stringify(removePokemon));
    if (removePokemon.length === 0) {
      localStorage.removeItem('my_pokemon');
    }
    window.location.reload();
  }

  return (
		<Layout>
      {myPokemonList.length > 0 ? <div className="my-pokemon-list">
      {
        myPokemonList.map((pokemon, idx) => (
            <div className="my-pokemon" key={idx}>
              <Button onClick={() => removePokemon(pokemon.name)}>X</Button>
              <div className="picture-pokemon">
                <img src={pokemon?.sprites?.other['official-artwork'].front_default} alt="" />
              </div>
              <h4 className="title">{pokemon.name}</h4>
            </div>
        ))
      }
      </div> : <div>There's no pokemon on your list.</div>}
    </Layout>
	);
}

export default MyPokemonList;