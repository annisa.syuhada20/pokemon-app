import PokemonList from "./PokemonList";
import PokemonDetail from "./PokemonDetail";
import MyPokemonList from "./MyPokemonList";

export { PokemonList, PokemonDetail, MyPokemonList };