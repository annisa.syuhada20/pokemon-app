import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from "react-router-dom";
import { PokemonList, PokemonDetail, MyPokemonList } from './pages';
import Nav from './layout/Nav';

function App () {
  return (
    <Router className="App">
      <Nav />
      <Switch>
        <Route exact path="/">
          <PokemonList />
        </Route>
        <Route exact path={`/pokemon-detail`}>
          <PokemonDetail />
        </Route>
        <Route exact path="/my-pokemon">
          <MyPokemonList />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
