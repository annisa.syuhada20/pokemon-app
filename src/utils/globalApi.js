import axios from 'axios';

const baseURL = 'https://pokeapi.co/api/v2'

export function getListPokemon () {
  const url = `${baseURL}/pokemon`;
  return axios.get(url)
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      return error.response?.data || [];
    });
}

export function getDetailPokemon (name) {
  const url = `${baseURL}/pokemon/${name}`;
  return axios.get(url)
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      return error.response?.data || {};
    });
}